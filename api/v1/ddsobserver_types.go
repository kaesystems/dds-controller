/*
Copyright 2022.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// DDSObserverSpec defines the desired state of DDSObserver
type SpecCheck int

const (
	SpecCheckSame      SpecCheck = 0
	SpecCheckDifferent SpecCheck = 1
)

type ElasticsearchStatus string

const (
	ElasticsearchStatusConnected ElasticsearchStatus = "Connected"
	ElasticsearchStatusFailed    ElasticsearchStatus = "Failed"
)

type ObserverStatus string

const (
	ObserverStatusRunning ObserverStatus = "Running"
	ObserverStatusFailed  ObserverStatus = "Failed"
)

type DDSObserverSpec struct {
	// INSERT ADDITIONAL SPEC FIELDS - desired state of cluster
	// Important: Run "make" to regenerate code after modifying this file

	// Foo is an example field of DDSObserver. Edit ddsobserver_types.go to remove/update
	DomainId          int    `json:"domainId,omitempty"`
	DiscoveryServerIp string `json:"discoveryServerIp,omitempty"`
	ElasticsearchURL  string `json:"elasticsearchUrl,omitempty"`
	PrometheusPort    int    `json:"prometheusPort,omitempty"`
	// Filter accepts a string array to filtered out topics according to topic name.
	Filter []string `json:"filter,omitempty"`
	// Add default keywords filter list.
	DefaultFilter bool `json:"defaultFilter,omitempty"`
}

// DDSObserverStatus defines the observed state of DDSObserver
type DDSObserverStatus struct {
	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "make" to regenerate code after modifying this file
	Status                  ObserverStatus      `json:"status,omitempty"`
	ElasticsearchConnection ElasticsearchStatus `json:"elasticsearchConnection,omitempty"`
	Topics                  []Topic             `json:"topics,omitempty"`
}
type TopicStatus struct {
	Timestamp int64   `json:"timestamp,omitempty"`
	Topics    []Topic `json:"topics,omitempty"`
}

type Topic struct {
	TopicName   string       `json:"topic_name,omitempty"`
	Latency     string       `json:"latency,omitempty"`
	Matched     string       `json:"matched,omitempty"`
	DataWriters []DataWriter `json:"data_writers"`
	DataReaders []DataReader `json:"data_readers"`
}
type DataWriter struct {
	DataWriterName string            `json:"data_writer_name"`
	Locator        []string          `json:"locator"`
	Metrics        DataWriterMetrics `json:"metrics"`
}
type DataReader struct {
	DataReaderName string            `json:"data_reader_name"`
	Locator        []string          `json:"locator"`
	Metrics        DataReaderMetrics `json:"metrics"`
}
type DataWriterMetrics struct {
	DataCount             string `json:"data_count"`
	GapCount              string `json:"gap_count"`
	HeartbeatCount        string `json:"heartbeat_count"`
	PublicationThroughput string `json:"publication_throughput"`
	ResentData            string `json:"resent_data"`
	SampleDatas           string `json:"sample_datas"`
}

type DataReaderMetrics struct {
	AcknackCount           string `json:"acknack_count"`
	NackfragCount          string `json:"nackfrag_count"`
	SubscriptionThroughput string `json:"subscription_throughput"`
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status

// DDSObserver is the Schema for the ddsobservers API
type DDSObserver struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   DDSObserverSpec   `json:"spec,omitempty"`
	Status DDSObserverStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true

// DDSObserverList contains a list of DDSObserver
type DDSObserverList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []DDSObserver `json:"items"`
}

func init() {
	SchemeBuilder.Register(&DDSObserver{}, &DDSObserverList{})
}
