/*
Copyright 2022.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// DDSRouterSpec defines the desired state of DDSRouter
type AllowedTopic struct {
	Name string `json:"name" yaml:"name"`
	Type string `json:"type" yaml:"type"`
}

type Address struct {
	//+kubebuilder:validation:Optional
	Ip string `json:"ip,omitempty" yaml:"ip,omitempty"`
	//+kubebuilder:validation:Optional
	Domain string `json:"domain,omitempty" yaml:"domain,omitempty"`

	Port      int    `json:"port" yaml:"port"`
	Transport string `json:"transport" yaml:"transport"`
}

type Configuration struct {
	Type string `json:"type" yaml:"type"`
	//+kubebuilder:validation:Optional
	ROSDiscoveryServer bool `json:"rosDiscoveryServer,omitempty" yaml:"ros-discovery-server,omitempty"`
	Id                 int  `json:"id" yaml:"id"`

	ListeningAddresses []Address `json:"listeningAddresses" yaml:"listening-addresses"`
}
type Router struct {
	AllowList []AllowedTopic `json:"allowlist,omitempty" yaml:"allowlist,omitempty"`
	//+kubebuilder:validation:Optional
	LocalDiscoveryServer Configuration `json:"LocalDiscoveryServer" yaml:"LocalRosDiscovery"`
	//+kubebuilder:validation:Optional
	CloudWAN Configuration `json:"CloudWAN,omitempty" yaml:"CloudWAN,omitempty"`
}
type DDSRouterSpec struct {
	// INSERT ADDITIONAL SPEC FIELDS - desired state of cluster
	// Important: Run "make" to regenerate code after modifying this file

	// Foo is an example field of DDSRouter. Edit ddsrouter_types.go to remove/update
	RouterSettings Router `json:"routerSettings"`
}

// DDSRouterStatus defines the observed state of DDSRouter
type DDSRouterStatus struct {
	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "make" to regenerate code after modifying this file
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status

// DDSRouter is the Schema for the ddsrouters API
type DDSRouter struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   DDSRouterSpec   `json:"spec,omitempty"`
	Status DDSRouterStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true

// DDSRouterList contains a list of DDSRouter
type DDSRouterList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []DDSRouter `json:"items"`
}

func init() {
	SchemeBuilder.Register(&DDSRouter{}, &DDSRouterList{})
}
