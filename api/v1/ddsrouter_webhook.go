/*
Copyright 2022.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1

import (
	"k8s.io/apimachinery/pkg/runtime"
	ctrl "sigs.k8s.io/controller-runtime"
	logf "sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/controller-runtime/pkg/webhook"
)

// log is for logging in this package.
var ddsrouterlog = logf.Log.WithName("ddsrouter-resource")

func (r *DDSRouter) SetupWebhookWithManager(mgr ctrl.Manager) error {
	return ctrl.NewWebhookManagedBy(mgr).
		For(r).
		Complete()
}

// TODO(user): EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!

//+kubebuilder:webhook:path=/mutate-dds-roboscale-io-v1-ddsrouter,mutating=true,failurePolicy=fail,sideEffects=None,groups=dds.roboscale.io,resources=ddsrouters,verbs=create;update,versions=v1,name=mddsrouter.kb.io,admissionReviewVersions=v1

var _ webhook.Defaulter = &DDSRouter{}

func isEmpty(server *Configuration) bool {
	// { false 0 []}
	return len(server.ListeningAddresses) <= 0
}

// Default implements webhook.Defaulter so a webhook will be registered for the type
func (r *DDSRouter) Default() {
	ddsrouterlog.Info("Set Default dds cluster logic", "name", r.Name)

	if isEmpty(&r.Spec.RouterSettings.LocalDiscoveryServer) {
		r.Spec.RouterSettings.LocalDiscoveryServer = Configuration{
			Id:                 0,
			Type:               "udp",
			ROSDiscoveryServer: true,
			ListeningAddresses: []Address{
				{
					Port:      30001,
					Transport: "udp",
				},
			},
		}
	}
	if isEmpty(&r.Spec.RouterSettings.CloudWAN) {
		r.Spec.RouterSettings.CloudWAN = Configuration{
			Id:                 0,
			Type:               "udp",
			ROSDiscoveryServer: true,
			ListeningAddresses: []Address{
				{
					Ip:        "0.0.0.0", // write your public id
					Port:      30002,
					Transport: "udp",
				},
			},
		}
	}

	// }
	// TODO(user): fill in your defaulting logic.
}

// TODO(user): change verbs to "verbs=create;update;delete" if you want to enable deletion validation.
//+kubebuilder:webhook:path=/validate-dds-roboscale-io-v1-ddsrouter,mutating=false,failurePolicy=fail,sideEffects=None,groups=dds.roboscale.io,resources=ddsrouters,verbs=create;update,versions=v1,name=vddsrouter.kb.io,admissionReviewVersions=v1

var _ webhook.Validator = &DDSRouter{}

// ValidateCreate implements webhook.Validator so a webhook will be registered for the type
func (r *DDSRouter) ValidateCreate() error {
	ddsrouterlog.Info("validate create", "name", r.Name)

	// TODO(user): fill in your validation logic upon object creation.
	return nil
}

// ValidateUpdate implements webhook.Validator so a webhook will be registered for the type
func (r *DDSRouter) ValidateUpdate(old runtime.Object) error {
	ddsrouterlog.Info("validate update", "name", r.Name)

	// TODO(user): fill in your validation logic upon object update.
	return nil
}

// ValidateDelete implements webhook.Validator so a webhook will be registered for the type
func (r *DDSRouter) ValidateDelete() error {
	ddsrouterlog.Info("validate delete", "name", r.Name)

	// TODO(user): fill in your validation logic upon object deletion.
	return nil
}
