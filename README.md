# DDS Controller

DDS Controller project contains following Resource and Controllers.

<table>
<thead>
<th>Group</th>
<th>Kind</th>
<th>Description</th>

</thead>
<tbody>
<tr><td>dds</td><td>DDS Observer</td><td>Responsible for collect DDS data and pushed them into Elasticsearch and Prometheus.</td></tr>
<tr><td>dds</td><td>DDS Router</td><td>Responsible to export DDS Message over Internet</td></tr>
</tbody>

</table>

## Test

The following command run tests when you first compile.

```
make test
```

After you can use the following command in <code>controller</code> directory

## Dev Environment

Changes for development purposes

- <code>imagePullPolicy: Always</code> to keep update container.
