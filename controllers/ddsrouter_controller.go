/*
Copyright 2022.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"

	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/util/intstr"

	"github.com/go-logr/logr"
	"gopkg.in/yaml.v3"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/log"

	ddsv1 "bitbucket.org/kaesystems/dds-controller/api/v1"
)

// DDSRouterReconciler reconciles a DDSRouter object
type DDSRouterReconciler struct {
	client.Client
	Scheme *runtime.Scheme
}

//+kubebuilder:rbac:groups=dds.roboscale.io,resources=ddsrouters,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=dds.roboscale.io,resources=ddsrouters/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=dds.roboscale.io,resources=ddsrouters/finalizers,verbs=update

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
// TODO(user): Modify the Reconcile function to compare the state specified by
// the DDSRouter object against the actual cluster state, and then
// perform operations to make the cluster state reflect the state specified by
// the user.
//
// For more details, check Reconcile and its Result here:
// - https://pkg.go.dev/sigs.k8s.io/controller-runtime@v0.10.0/pkg/reconcile

//The following function is reponsible to create configmap to manage DDSRouter
const (
	InternalService string = "internal"
	CloudService    string = "external"
)

func ConstructConfigMap(ddsRouter *ddsv1.DDSRouter, log logr.Logger) (*corev1.ConfigMap, error) {
	if ddsRouter.Spec.RouterSettings.LocalDiscoveryServer.ListeningAddresses[0].Domain == "" {
		log.Info("Service domain not found. It will be created!")
		ddsRouter.Spec.RouterSettings.LocalDiscoveryServer.ListeningAddresses[0].Domain = ddsRouter.Name + "-internal." + ddsRouter.Namespace + ".svc.cluster.local"
	}
	if ddsRouter.Spec.RouterSettings.CloudWAN.ListeningAddresses[0].Ip == "" {
		log.Info("Cluster public ip is not specified")
	}
	object, err := yaml.Marshal(&ddsRouter.Spec.RouterSettings)
	if err != nil {
		return nil, err
	}
	configMap := &corev1.ConfigMap{
		ObjectMeta: metav1.ObjectMeta{
			Name:      ddsRouter.Name,
			Namespace: ddsRouter.Namespace,
			Labels:    ddsRouter.ObjectMeta.Labels,
		},
		Data: map[string]string{
			"configurationData": string(object),
		},
	}
	return configMap, nil
}

func ConstructRouterPod(ddsRouter *ddsv1.DDSRouter) *corev1.Pod {
	pod := &corev1.Pod{
		ObjectMeta: metav1.ObjectMeta{
			Name:      ddsRouter.Name,
			Namespace: ddsRouter.Namespace,
			Labels: map[string]string{
				//TODO: Generate label
				"app": ddsRouter.Name,
			},
		},

		Spec: corev1.PodSpec{
			Volumes: []corev1.Volume{
				{
					Name: "config",
					VolumeSource: corev1.VolumeSource{
						ConfigMap: &corev1.ConfigMapVolumeSource{
							LocalObjectReference: corev1.LocalObjectReference{Name: ddsRouter.Name},
							Items: []corev1.KeyToPath{
								{
									Key:  "configurationData",
									Path: "DDSROUTER_CONFIGURATION.yaml",
								},
							},
						},
					},
				},
			},
			RestartPolicy: "Always",
			Containers: []corev1.Container{
				{
					Name:  "dds-router",
					Image: "robolaunchio/dds-router:v1",
					Ports: []corev1.ContainerPort{
						{
							Name:          "internal-port",
							Protocol:      "UDP",
							ContainerPort: int32(ddsRouter.Spec.RouterSettings.LocalDiscoveryServer.ListeningAddresses[0].Port),
						},
						{
							Name:          "external-port",
							Protocol:      "UDP",
							ContainerPort: int32(ddsRouter.Spec.RouterSettings.CloudWAN.ListeningAddresses[0].Port),
						},
					},
					VolumeMounts: []corev1.VolumeMount{
						{
							Name:      "config",
							MountPath: "/ddsrouter/resources",
						},
					},
				},
			},
		},
	}
	return pod
}
func ConstructService(router *ddsv1.DDSRouter, serviceSettings string) *corev1.Service {
	var port int
	var serviceType string
	var portSettings corev1.ServicePort
	var objectMeta metav1.ObjectMeta
	if serviceSettings == InternalService {
		objectMeta =
			metav1.ObjectMeta{
				Name:      router.Name + "-internal",
				Namespace: router.Namespace,
				Labels:    router.ObjectMeta.Labels,
			}
		port = router.Spec.RouterSettings.LocalDiscoveryServer.ListeningAddresses[0].Port
		serviceType = "ClusterIP"
		portSettings = corev1.ServicePort{
			Protocol:   corev1.ProtocolUDP,
			Name:       "udp-tunnel",
			TargetPort: intstr.FromInt(port),
			Port:       int32(port),
		}
	} else if serviceSettings == CloudService {
		objectMeta =
			metav1.ObjectMeta{
				Name:      router.Name + "-cloud",
				Namespace: router.Namespace,
				Labels:    router.ObjectMeta.Labels,
			}
		port = router.Spec.RouterSettings.CloudWAN.ListeningAddresses[0].Port
		serviceType = "NodePort"
		portSettings = corev1.ServicePort{
			Protocol:   corev1.ProtocolUDP,
			Name:       "udp-tunnel",
			TargetPort: intstr.FromInt(port),
			Port:       int32(port),
		}
	}
	service := &corev1.Service{
		ObjectMeta: objectMeta,
		Spec: corev1.ServiceSpec{
			Selector: map[string]string{
				"app": router.Name,
			},
			Type: corev1.ServiceType(serviceType),
			Ports: []corev1.ServicePort{
				portSettings,
			},
		},
	}
	return service
}
func (r *DDSRouterReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	log := log.FromContext(ctx)

	var ddsRouter ddsv1.DDSRouter
	if err := r.Get(ctx, req.NamespacedName, &ddsRouter); err != nil {
		log.Info(req.Name + "is deleted!")
		return ctrl.Result{}, client.IgnoreNotFound(err)
	}

	configMap := &corev1.ConfigMap{}
	err := r.Get(ctx, types.NamespacedName{Name: ddsRouter.Name, Namespace: ddsRouter.Namespace}, configMap)
	if err != nil && errors.IsNotFound(err) {
		//TODO: Create configmap
		configMap, err = ConstructConfigMap(&ddsRouter, log)
		if err != nil {
			log.Info("Construction failed")
			return ctrl.Result{}, err
		}
		if err := ctrl.SetControllerReference(&ddsRouter, configMap, r.Scheme); err != nil {
			log.Info("Owner reference not assigned")
			return ctrl.Result{}, err
		}
		if err := r.Create(ctx, configMap); err != nil {
			log.Info("Configmap creation process failed!")
			return ctrl.Result{}, err
		}
	}
	pod := &corev1.Pod{}
	err = r.Get(ctx, types.NamespacedName{Name: ddsRouter.Name, Namespace: ddsRouter.Namespace}, pod)
	if err != nil && errors.IsNotFound(err) {
		//TODO: Create configmap
		pod = ConstructRouterPod(&ddsRouter)
		if err := ctrl.SetControllerReference(&ddsRouter, pod, r.Scheme); err != nil {
			log.Info("Pod owner reference not assigned")
			return ctrl.Result{}, err
		}
		if err := r.Create(ctx, pod); err != nil {
			log.Info("Pod creation process failed!")
			return ctrl.Result{}, err
		}
	}

	internalService := &corev1.Service{}
	err = r.Get(ctx, types.NamespacedName{Name: ddsRouter.Name + "-internal", Namespace: ddsRouter.Namespace}, internalService)
	if err != nil && errors.IsNotFound(err) {
		log.Info("Internal Service not found it will be created!")
		internalService = ConstructService(&ddsRouter, InternalService)

		if err := ctrl.SetControllerReference(&ddsRouter, internalService, r.Scheme); err != nil {
			log.Info("Internal Service controller reference cannot set.")
			return ctrl.Result{}, err
		}
		if err := r.Create(ctx, internalService); err != nil {
			log.Info("Internal Service creation failed.")
			return ctrl.Result{}, err
		}
	}
	cloudService := &corev1.Service{}
	err = r.Get(ctx, types.NamespacedName{Name: ddsRouter.Name + "-cloud", Namespace: ddsRouter.Namespace}, cloudService)
	if err != nil && errors.IsNotFound(err) {
		log.Info("Cloud Service not found it will be created!")
		cloudService = ConstructService(&ddsRouter, CloudService)

		if err := ctrl.SetControllerReference(&ddsRouter, cloudService, r.Scheme); err != nil {
			log.Info("Cloud Service controller reference cannot set.")
			return ctrl.Result{}, err
		}
		if err := r.Create(ctx, cloudService); err != nil {
			log.Info("Cloud Service creation failed.")

			return ctrl.Result{}, err
		}
	}
	return ctrl.Result{}, nil
}

// SetupWithManager sets up the controller with the Manager.
func (r *DDSRouterReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&ddsv1.DDSRouter{}).
		Owns(&corev1.ConfigMap{}).
		Owns(&corev1.Pod{}).
		Owns(&corev1.Service{}).
		Complete(r)
}
