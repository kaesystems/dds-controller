package controllers

import (
	"context"
	"fmt"
	"time"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	corev1 "k8s.io/api/core/v1"

	ddsv1 "bitbucket.org/kaesystems/dds-controller/api/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
)

var _ = Describe("DDSObserver controller", func() {

	// Define utility constants for object names and testing timeouts/durations and intervals.
	const (
		DDSObserverName      = "ddsobservertest"
		DDSObserverNamespace = "default"

		elasticsearchURL = "http://elastic.robolaunch.cloud"
		fakeElastic      = "http://elasticas.robolaunch.cloud"

		timeout  = time.Second * 5
		duration = time.Second * 5
		interval = time.Millisecond * 250
	)
	Context("When creating the resource from scratch is this issued by controller.", func() {
		It("Elasticsearch status expected Connected", func() {
			By("By creating a new DDSObserver")
			ctx := context.Background()
			ddsObserver := &ddsv1.DDSObserver{
				TypeMeta: metav1.TypeMeta{
					APIVersion: "dds.roboscale.io/v1",
					Kind:       "DDSObserver",
				},
				ObjectMeta: metav1.ObjectMeta{
					Name:      DDSObserverName,
					Namespace: DDSObserverNamespace,
				},
				Spec: ddsv1.DDSObserverSpec{
					ElasticsearchURL: elasticsearchURL,
				},
			}
			Expect(k8sClient.Create(ctx, ddsObserver)).Should(Succeed())
			// By("By checking Status of object")

			Eventually(func() bool {
				ddsResult := &ddsv1.DDSObserver{}

				if err := k8sClient.Get(ctx, types.NamespacedName{Name: DDSObserverName, Namespace: DDSObserverNamespace}, ddsResult); err != nil {
					return false
				}
				return true
			}, timeout, interval).Should(BeTrue())
			//FIXME: Status check needs container run
			// Eventually(func() string {
			// 	testDds := &ddsv1.DDSObserver{}
			// 	if err := k8sClient.Get(ctx, types.NamespacedName{Name: DDSObserverName, Namespace: DDSObserverNamespace}, testDds); err != nil {
			// 		Fail(err.Error())
			// 	}
			// 	fmt.Println(testDds.Status.Status)
			// 	return testDds.Status.Status
			// }, duration*5, interval).Should(BeEquivalentTo("Running"))
			Eventually(func() ddsv1.ElasticsearchStatus {
				testDds := &ddsv1.DDSObserver{}
				if err := k8sClient.Get(ctx, types.NamespacedName{Name: DDSObserverName, Namespace: DDSObserverNamespace}, testDds); err != nil {
					Fail(err.Error())
				}
				return testDds.Status.ElasticsearchConnection
			}, duration*2, interval).Should(BeEquivalentTo("Connected"))

		})
		It("After update on resource pod must be deleted for to re-create and elasticsearch state should be turn to failed", func() {
			ddsObserver := &ddsv1.DDSObserver{}
			k8sClient.Get(ctx, types.NamespacedName{Name: DDSObserverName, Namespace: DDSObserverNamespace}, ddsObserver)

			ddsObserver.Spec.ElasticsearchURL = fakeElastic
			Expect(k8sClient.Update(ctx, ddsObserver)).Should(Succeed())
			//Be sure resource created
			k8sClient.Get(ctx, types.NamespacedName{Namespace: DDSObserverNamespace, Name: DDSObserverName}, ddsObserver)
			print(ddsObserver)
			// Status should be fail!
			// Eventually(func() string {
			// 	pod := &corev1.Pod{}
			// 	if err := k8sClient.Get(ctx, types.NamespacedName{Name: DDSObserverName, Namespace: DDSObserverNamespace}, pod); err != nil {
			// 		Fail(err.Error())
			// 	}
			// 	fmt.Println(pod.Status)
			// 	return "NotFailed"
			// }, duration, interval).Should(BeEquivalentTo("Failed"))
			Eventually(func() ddsv1.ElasticsearchStatus {
				testDds := &ddsv1.DDSObserver{}
				if err := k8sClient.Get(ctx, types.NamespacedName{Name: DDSObserverName, Namespace: DDSObserverNamespace}, testDds); err != nil {
					Fail(err.Error())
				}
				return testDds.Status.ElasticsearchConnection
			}, duration*15, interval).Should(BeEquivalentTo("Failed"))
		})
		It("If filter has an element, expect as args in pods", func() {
			ddsObserver := &ddsv1.DDSObserver{}
			k8sClient.Get(ctx, types.NamespacedName{Name: DDSObserverName, Namespace: DDSObserverNamespace}, ddsObserver)

			ddsObserver.Spec.Filter = []string{
				"Hello",
				"Test",
			}
			Expect(k8sClient.Update(ctx, ddsObserver)).Should(Succeed())
			//Be sure resource created
			k8sClient.Get(ctx, types.NamespacedName{Namespace: DDSObserverNamespace, Name: DDSObserverName}, ddsObserver)
			print(ddsObserver)

			Eventually(func() string {
				pod := &corev1.Pod{}
				var arg string
				if err := k8sClient.Get(ctx, types.NamespacedName{Name: DDSObserverName, Namespace: DDSObserverNamespace}, pod); err != nil {
					return "Error try again"
				}
				//check pod owner reference
				fmt.Println("I tried :(")
				for i := 0; i < len(pod.Spec.Containers[0].Command); i++ {
					if pod.Spec.Containers[0].Command[i] == "--topic_filter" {
						arg = pod.Spec.Containers[0].Command[i+1]
					}
				}
				return arg
			}, duration*2, interval).Should(BeEquivalentTo("Hello,Test"))
		})
	})

})
