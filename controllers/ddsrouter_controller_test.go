package controllers

import (
	"context"
	"fmt"
	"time"

	ddsv1 "bitbucket.org/kaesystems/dds-controller/api/v1"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
)

var _ = Describe("DDSRouter controller", func() {

	// Define utility constants for object names and testing timeouts/durations and intervals.
	const (
		DDSRouterName      = "ddsroutertest"
		DDSRouterNamespace = "default"

		timeout  = time.Second * 5
		duration = time.Second * 5
		interval = time.Millisecond * 250
	)
	Context("When creating DDSRouter from scratch is this issued by controller.", func() {
		It("Be sure all resources are created as expected", func() {
			By("By creating a new DDSObserver")
			ctx := context.Background()
			ddsObserver := &ddsv1.DDSRouter{
				TypeMeta: metav1.TypeMeta{
					APIVersion: "dds.roboscale.io/v1",
					Kind:       "DDSRouter",
				},
				ObjectMeta: metav1.ObjectMeta{
					Name:      DDSRouterName,
					Namespace: DDSRouterNamespace,
				},
				Spec: ddsv1.DDSRouterSpec{
					RouterSettings: ddsv1.Router{
						AllowList: []ddsv1.AllowedTopic{
							{
								Name: "rt/test",
								Type: "std_msgs::msg::dds_::String_",
							},
							{
								Name: "rt/test",
								Type: "std_msgs::msg::dds_::String_",
							},
						},
						LocalDiscoveryServer: ddsv1.Configuration{
							Type:               "local-discovery-server",
							ROSDiscoveryServer: true,
							Id:                 0,
							ListeningAddresses: []ddsv1.Address{
								{
									Port:      31234,
									Transport: "udp",
								},
							},
						},
						CloudWAN: ddsv1.Configuration{
							Type: "wan",
							Id:   2,
							ListeningAddresses: []ddsv1.Address{
								{
									Ip:        "23.88.62.179",
									Port:      31235,
									Transport: "udp",
								},
							},
						},
					},
				},
			}
			Expect(k8sClient.Create(ctx, ddsObserver)).Should(Succeed())

			Eventually(func() bool {
				//Be sure pod created
				pod := &corev1.Pod{}
				if err := k8sClient.Get(ctx, types.NamespacedName{Name: DDSRouterName, Namespace: DDSRouterNamespace}, pod); err != nil {
					fmt.Println(err)
					return false
				}
				return true
			}, timeout*10, interval).Should(BeTrue())

			Eventually(func() bool {
				//Be sure pod created
				internalService := &corev1.Service{}

				if err := k8sClient.Get(ctx, types.NamespacedName{Name: DDSRouterName + "-internal", Namespace: DDSRouterNamespace}, internalService); err != nil {
					fmt.Println(err)
					return false
				}
				return true
			}, timeout*2, interval).Should(BeTrue())

			Eventually(func() bool {
				//Be sure pod created
				cloudService := &corev1.Service{}

				if err := k8sClient.Get(ctx, types.NamespacedName{Name: DDSRouterName + "-cloud", Namespace: DDSRouterNamespace}, cloudService); err != nil {
					return false
				}
				return true
			}, timeout*2, interval).Should(BeTrue())

			Eventually(func() bool {
				//Be sure pod created
				configMap := &corev1.ConfigMap{}

				if err := k8sClient.Get(ctx, types.NamespacedName{Name: DDSRouterName, Namespace: DDSRouterNamespace}, configMap); err != nil {
					fmt.Println(err)
					return false
				}
				return true
			}, timeout, interval).Should(BeTrue())
			// Eventually(func() string {
			// 	testDds := &ddsv1.DDSObserver{}
			// 	if err := k8sClient.Get(ctx, types.NamespacedName{Name: DDSRouterName, Namespace: DDSRouterNamespace}, testDds); err != nil {
			// 		Fail(err.Error())
			// 	}
			// 	return testDds.Status.ElasticsearchConnection
			// }, duration*2, interval).Should(BeEquivalentTo("Connected"))

		})
	})
})
