/*
Copyright 2022.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"reflect"
	"time"

	ddsv1 "bitbucket.org/kaesystems/dds-controller/api/v1"
	"bitbucket.org/kaesystems/dds-controller/pkg/resource"
	"bitbucket.org/kaesystems/dds-controller/pkg/utils"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/client-go/tools/record"

	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/log"
)

// DDSObserverReconciler reconciles a DDSObserver object
type DDSObserverReconciler struct {
	client.Client
	Scheme   *runtime.Scheme
	recorder record.EventRecorder
}

//+kubebuilder:rbac:groups=dds.roboscale.io,resources=ddsobservers,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=dds.roboscale.io,resources=ddsobservers/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=dds.roboscale.io,resources=ddsobservers/finalizers,verbs=update
//+kubebuilder:rbac:groups=core,resources=pods,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=core,resources=services,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=core,resources=configmaps,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=core,resources=events,verbs=get;list;watch;create;update;patch;delete

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
// TODO(user): Modify the Reconcile function to compare the state specified by
// the DDSObserver object against the actual cluster state, and then
// perform operations to make the cluster state reflect the state specified by
// the user.
//
// For more details, check Reconcile and its Result here:
// - https://pkg.go.dev/sigs.k8s.io/controller-runtime@v0.10.0/pkg/reconcile

func (r *DDSObserverReconciler) StatusCheck(observer *ddsv1.DDSObserver, pod *corev1.Pod, ctx context.Context) error {
	oldStatus := observer.Status.Status
	if len(pod.Status.ContainerStatuses) != 0 && pod.Status.ContainerStatuses[0].Ready && observer.Status.Status != "Running" {
		r.recorder.Event(observer, corev1.EventTypeNormal, "Running", "DDSObserver pod running.")
		observer.Status.Status = "Running"
	}
	if len(pod.Status.ContainerStatuses) != 0 && !pod.Status.ContainerStatuses[0].Ready {
		observer.Status.Status = "Failed"
	}
	if oldStatus != observer.Status.Status {
		if err := r.Status().Update(ctx, observer); err != nil {
			return err
		}
	}
	return nil
}

func (r *DDSObserverReconciler) CheckSpecs(observer *ddsv1.DDSObserver, pod *corev1.Pod, ctx context.Context) (ddsv1.SpecCheck, error) {
	if !reflect.DeepEqual(utils.ParseArgs(observer), pod.Spec.Containers[0].Command) {
		r.recorder.Event(observer, corev1.EventTypeNormal, "Changed", "DDSObserver resource configuration changed!")
		if err := r.Delete(ctx, pod); err != nil {
			return -1, err
		}
		//TODO: clean status flags!
		observer.Status.ElasticsearchConnection = ""
		observer.Status.Status = ""
		r.recorder.Event(observer, corev1.EventTypeNormal, "Delete", "DDSObserver pod deleted.")
		r.Status().Update(ctx, observer)

		return ddsv1.SpecCheckDifferent, nil
	}
	return ddsv1.SpecCheckSame, nil
}
func (r *DDSObserverReconciler) CreateSubresource(observer *ddsv1.DDSObserver, obj client.Object, ctx context.Context) error {
	if err := ctrl.SetControllerReference(observer, obj, r.Scheme); err != nil {
		return err
	}
	if err := r.Create(ctx, obj); err != nil {
		return err
	}
	return nil
}

func (r *DDSObserverReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	var observer ddsv1.DDSObserver
	var pod corev1.Pod
	var service corev1.Service
	var builder resource.ObserverResources

	log := log.FromContext(ctx)

	if err := r.Get(ctx, req.NamespacedName, &observer); err != nil {
		log.Info(req.Name + " is deleted!")
		return ctrl.Result{}, client.IgnoreNotFound(err)
	}

	if observer.Status.ElasticsearchConnection != "Connected" {
		if err := utils.CheckConnection(observer.Spec.ElasticsearchURL); err != nil {
			observer.Status.ElasticsearchConnection = ddsv1.ElasticsearchStatusFailed
			r.recorder.Event(&observer, corev1.EventTypeWarning, "ConnectionError", "Elasticsearch is not reachable.")
			log.Error(err, "Error happend to reach elasticsearch: "+observer.Spec.ElasticsearchURL)

		} else {
			observer.Status.ElasticsearchConnection = ddsv1.ElasticsearchStatusConnected
		}
	}
	//TODO: Update pod according to info
	//TODO: Check pod value and
	if err := r.Get(ctx, req.NamespacedName, &pod); err != nil {
		log.Info(req.Name + " is not found!")
		builder.CreatePod(req.NamespacedName, utils.ParseArgs(&observer)...)
		pod, err := builder.GetPod()
		if err != nil {
			return ctrl.Result{}, err
		}
		if err := r.CreateSubresource(&observer, pod, ctx); err != nil {
			return ctrl.Result{}, err
		}
		r.recorder.Event(&observer, corev1.EventTypeNormal, "Created", "DDSObserver pod succesfully created!")
		return ctrl.Result{}, nil
	}

	if err := r.Get(ctx, req.NamespacedName, &service); err != nil {
		log.Info(req.Name + " is not found!")
		builder.CreateService(req.NamespacedName)
		service, err := builder.GetService()
		if err != nil {
			return ctrl.Result{}, err
		}

		if err := r.CreateSubresource(&observer, service, ctx); err != nil {
			return ctrl.Result{}, err
		}
		r.recorder.Event(&observer, corev1.EventTypeNormal, "Created", "DDSObserver service succesfully created!")

		return ctrl.Result{}, nil
	}
	check, err := r.CheckSpecs(&observer, &pod, ctx)
	if err != nil {
		return ctrl.Result{}, err
	}
	if check == ddsv1.SpecCheckDifferent {
		log.Info("Pod deleted for updating with new one")
		return ctrl.Result{}, nil
	}

	if err := r.StatusCheck(&observer, &pod, ctx); err != nil {
		log.Error(err, "Error happend when update")
		return ctrl.Result{}, nil
	}
	topicStatus, err := utils.GetDataFromService("http://" + req.Name + "." + req.Namespace + ".svc.cluster.local:5500")
	if err != nil {
		log.Info(err.Error())
		r.recorder.Event(&observer, corev1.EventTypeWarning, "FetchError", "Unable to fetch statistics.")
		r.Status().Update(ctx, &observer)
		return ctrl.Result{RequeueAfter: 3 * time.Second}, nil
	}

	topics := topicStatus.Topics
	if !reflect.DeepEqual(observer.Status.Topics, topics) && !(len(observer.Status.Topics) == 0 && len(topics) == 0) {
		observer.Status.Topics = topics
		r.recorder.Event(&observer, corev1.EventTypeNormal, "Update", "Topics data updated.")
		if err := r.Status().Update(ctx, &observer); err != nil {
			log.Error(err, "Error happend when update")
			return ctrl.Result{RequeueAfter: 2 * time.Second}, nil
		}
		log.Info("Topics are changed!!")
	}
	return ctrl.Result{RequeueAfter: 2 * time.Second}, nil
}

// SetupWithManager sets up the controller with the Manager.
func (r *DDSObserverReconciler) SetupWithManager(mgr ctrl.Manager) error {
	//The following line added for recording event.
	r.recorder = mgr.GetEventRecorderFor("DDSObserver")
	return ctrl.NewControllerManagedBy(mgr).
		For(&ddsv1.DDSObserver{}).
		Owns(&corev1.Pod{}).
		Owns(&corev1.Service{}).
		Complete(r)
}
