package utils_test

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"reflect"
	"strconv"
	"testing"

	ddsv1 "bitbucket.org/kaesystems/dds-controller/api/v1"
	"bitbucket.org/kaesystems/dds-controller/pkg/utils"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

const (
	fakeName            string = "unitName"
	fakeNamespace       string = "unitNamespace"
	fakeElasticsearch   string = "http://null.io"
	fakeDiscoveryServer string = "0.0.0.0"
	fakePrometheusPort  int    = 8000
	fakeDomainID        int    = 1
	filter1             string = "Hello"
	filter2             string = "DDS"
	fakeDefault         bool   = true
)

func TestParseArgs(t *testing.T) {
	expected := []string{"./DDSProject", "--elasticsearch", fakeElasticsearch, "--ros_discovery_ip", fakeDiscoveryServer, "--prometheus_port", strconv.Itoa(fakePrometheusPort), "--domain_id", strconv.Itoa(fakeDomainID), "--topic_filter_default", "--topic_filter", filter1 + "," + filter2}
	//Create fake observer object
	observer := &ddsv1.DDSObserver{
		ObjectMeta: metav1.ObjectMeta{
			Name:      fakeName,
			Namespace: fakeNamespace,
		},
		Spec: ddsv1.DDSObserverSpec{
			ElasticsearchURL:  fakeElasticsearch,
			DiscoveryServerIp: fakeDiscoveryServer,
			DomainId:          fakeDomainID,
			Filter:            []string{filter1, filter2},
			PrometheusPort:    fakePrometheusPort,
			DefaultFilter:     fakeDefault,
		},
	}
	result := utils.ParseArgs(observer)
	t.Log(result)
	t.Log(expected)
	if !reflect.DeepEqual(result, expected) {
		t.Errorf("Expected array and result not same!\n")
	}
}

func TestCheckConnection(t *testing.T) {

	svr := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "Hello")
	}))
	err := utils.CheckConnection(svr.URL)
	if err != nil {
		t.Errorf("expected err to be nil got %v", err)
	}
	svr.Close()
	if err := utils.CheckConnection(svr.URL); err == nil {
		t.Errorf("the service should not avaliable %v", err)

	}
	svrNotAvailable := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(400)
		fmt.Fprintf(w, "Hello")
	}))
	err = utils.CheckConnection(svrNotAvailable.URL)
	if err == nil {
		t.Errorf("expected err to be nil got %v", err)
	}
	svrNotAvailable.Close()

	// res: expected\r\n
	// due to the http protocol cleanup response

}

func TestGetDataFromService(t *testing.T) {
	expected := ddsv1.TopicStatus{
		Timestamp: 0,
		Topics: []ddsv1.Topic{{
			TopicName: "fakeTopic",
			Latency:   "200.000",
			Matched:   "true",
			DataWriters: []ddsv1.DataWriter{
				{
					DataWriterName: "fake",
					Locator:        []string{"UDP:[0.0.0.0]:5055"},
				}},
		}},
	}

	svr := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		json.NewEncoder(w).Encode(&expected)
	}))
	fakeResult, err := utils.GetDataFromService(svr.URL)
	if err != nil {
		t.Errorf("expected err to be nil got %v", err)
	}
	t.Log(*fakeResult)
	t.Log(expected)
	if !reflect.DeepEqual(*fakeResult, expected) {
		t.Errorf("expected object is not equal actual object\n")
	}
	svr.Close()
	_, err = utils.GetDataFromService(svr.URL)
	if err == nil {
		t.Errorf("the service should not avaliable %v", err)

	}
	corruptObj := []ddsv1.Topic{{
		TopicName: "fakeTopic",
		Latency:   "200.000",
		Matched:   "true",
		DataWriters: []ddsv1.DataWriter{
			{
				DataWriterName: "fake",
				Locator:        []string{"UDP:[0.0.0.0]:5055"},
			}},
	}}
	corruptSv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		json.NewEncoder(w).Encode(&corruptObj)
	}))
	_, err = utils.GetDataFromService(corruptSv.URL)
	if err == nil {
		t.Errorf("message should not be read.\n")
	}
	// res: expected\r\n
	// due to the http protocol cleanup response

}
