package utils

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"

	ddsv1 "bitbucket.org/kaesystems/dds-controller/api/v1"
)

func GetDataFromService(url string) (*ddsv1.TopicStatus, error) {
	var topicStatus ddsv1.TopicStatus
	request, err := http.Get(url)
	//If not check request
	if err != nil {
		return nil, err
	}
	body, _ := ioutil.ReadAll(request.Body)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(body, &topicStatus)
	if err != nil {
		return nil, err

	}
	return &topicStatus, nil
}

func CheckConnection(url string) error {
	resp, err := http.Get(url)
	if err != nil {
		return errors.New("connection failed")
	} else {
		if resp.StatusCode == 200 {
			return nil
		}
		return errors.New("connection failed")
	}
}

func ParseArgs(observer *ddsv1.DDSObserver) []string {
	args := []string{"./DDSProject"}
	if observer.Spec.ElasticsearchURL != "" {
		args = append(args, "--elasticsearch", observer.Spec.ElasticsearchURL)
	}
	if observer.Spec.DiscoveryServerIp != "" {
		args = append(args, "--ros_discovery_ip", observer.Spec.DiscoveryServerIp)
	}
	if observer.Spec.PrometheusPort != 0 {
		args = append(args, "--prometheus_port", strconv.Itoa(observer.Spec.PrometheusPort))
	}
	if observer.Spec.DomainId != 0 {
		args = append(args, "--domain_id", strconv.Itoa(observer.Spec.DomainId))
	}
	if observer.Spec.DefaultFilter {
		args = append(args, "--topic_filter_default")
	}
	if len(observer.Spec.Filter) != 0 {
		//TODO: Merge strings
		merged := strings.Join(observer.Spec.Filter, ",")

		args = append(args, "--topic_filter", merged)
	}
	return args
}
