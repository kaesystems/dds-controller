package resource

import (
	"errors"

	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/intstr"

	"k8s.io/apimachinery/pkg/types"
)

// type IResources interface {
// 	GetPod() (*corev1.Pod, error)
// 	GetService() (*corev1.Service, error)
// }

type ObserverResources struct {
	// IResources
	pod     *corev1.Pod
	service *corev1.Service
}

// type RouterResources struct {
// 	IResources
// 	pod     *corev1.Pod
// 	service *corev1.Service
// }

// const (
// 	InternalService string = "internal"
// 	CloudService    string = "external"
// )

func (o *ObserverResources) CreatePod(meta types.NamespacedName, args ...string) {
	o.pod = &corev1.Pod{
		ObjectMeta: metav1.ObjectMeta{
			Name:      meta.Name,
			Namespace: meta.Namespace,

			Labels: map[string]string{
				"app": meta.Name,
			},
		},
		Spec: corev1.PodSpec{
			Containers: []corev1.Container{
				{
					Name:            "dds-statistics-service",
					Image:           "robolaunchio/dds-statistics-service",
					Command:         args,
					ImagePullPolicy: corev1.PullAlways,
				},
				{
					Name:            "dds-control-service",
					Image:           "robolaunchio/dds-control-service",
					ImagePullPolicy: corev1.PullAlways,
					Ports: []corev1.ContainerPort{
						{
							ContainerPort: 5500,
						},
					},
				},
			},
		},
	}
}

func (o *ObserverResources) CreateService(meta types.NamespacedName, opts ...string) {
	o.service = &corev1.Service{
		ObjectMeta: metav1.ObjectMeta{
			Name:      meta.Name,
			Namespace: meta.Namespace,
		},
		Spec: corev1.ServiceSpec{
			Selector: map[string]string{
				"app": meta.Name,
			},
			Type: corev1.ServiceTypeClusterIP,
			Ports: []corev1.ServicePort{
				{
					Name:       "watcher",
					TargetPort: intstr.FromInt(5500),
					Port:       5500,
					Protocol:   corev1.ProtocolTCP,
				},
			},
		},
	}
}

func (o *ObserverResources) GetPod() (*corev1.Pod, error) {
	if o.pod == nil {
		return nil, errors.New("pod not constucted")
	}
	return o.pod, nil
}

func (o *ObserverResources) GetService() (*corev1.Service, error) {
	if o.service == nil {
		return nil, errors.New("service not constucted")
	}
	return o.service, nil
}
