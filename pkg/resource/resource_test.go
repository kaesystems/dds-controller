package resource_test

import (
	"reflect"
	"testing"

	"bitbucket.org/kaesystems/dds-controller/pkg/resource"
	"k8s.io/apimachinery/pkg/types"
)

const (
	testName      string = "UnitTestName"
	testNamespace string = "UnitTestNs"
)

func TestCreateServiceAndGetService(t *testing.T) {

	builder := resource.ObserverResources{}
	_, err := builder.GetService()
	if err == nil {
		t.Errorf("Error expected! \n")
	}
	builder.CreateService(types.NamespacedName{Name: testName, Namespace: testNamespace})
	service, err := builder.GetService()
	if err != nil {
		t.Errorf("Error happend: %v\n", err)
	}
	if service.Name != testName {
		t.Errorf("Expected: %v Actualy: %v\n", testName, service.Name)
	}
	if service.Namespace != testNamespace {
		t.Errorf("Expected: %v Actualy: %v\n", testNamespace, service.Namespace)
	}

}

func TestCreatePodAndGetPod(t *testing.T) {
	builder := resource.ObserverResources{}
	_, err := builder.GetPod()
	if err == nil {
		t.Errorf("Error expected! \n")
	}
	var testArgs []string = []string{"--flag", "test"}
	builder.CreatePod(types.NamespacedName{Name: testName, Namespace: testNamespace}, testArgs...)
	pod, err := builder.GetPod()
	if err != nil {
		t.Errorf("Error happend: %v\n", err)
	}
	if pod.Name != testName {
		t.Errorf("Expected: %v Actualy: %v\n", testName, pod.Name)
	}
	if pod.Namespace != testNamespace {
		t.Errorf("Expected: %v Actualy: %v\n", testNamespace, pod.Namespace)
	}
	if !reflect.DeepEqual(testArgs, pod.Spec.Containers[0].Command) {
		t.Errorf("Expected: %v Actualy: %v\n", testArgs, pod.Spec.Containers[0].Command)
	}
}
